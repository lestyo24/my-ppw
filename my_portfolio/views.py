from django.shortcuts import render
from django.core.wsgi import get_wsgi_application


mhs_title = 'My Portfolio'
def index(request):
    response = {'name': mhs_title}
    return render(request, 'my_portfolio.html', response)
