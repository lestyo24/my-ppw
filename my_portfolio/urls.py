from django.urls import re_path
from .views import index
#url for app
app_name = 'my_portfolio'

urlpatterns = [
    re_path(r'^$', index, name='index'),
]
