from django.urls import re_path
from .views import my_view

app_name = 'login'

#url for app
urlpatterns = [
    re_path(r'^login', my_view, name='login')
]
