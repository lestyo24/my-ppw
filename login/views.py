from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
# Create your views here.

def my_view(request):
	username = request.POST['username']
	password = request.POST['password']
	
	user = authenticate(request, username=username,password=password)
	if user is not None:
		if user.is_active:
			response = {}
			request.session['user'] = user.username
			response['username'] = user.username
			login(request, user)
			return render(request, 'registration/logged-in.html', response)
		
	else:
		return HttpResponse('403')
