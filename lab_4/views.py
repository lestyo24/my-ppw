from django.shortcuts import render
from django.core.wsgi import get_wsgi_application

	
mhs_title = 'Live Chat'
def index(request):
    response = {'name': mhs_title}
    return render(request, 'lab_4/templates/lab_4.html', response)
