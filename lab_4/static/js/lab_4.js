$(document).ready(function(){
// kode jQuery selanjutnya akan ditulis disini

	$('textarea#text').on('keypress',function(e) {
	    if(e.which == 13) {
	        var text = $(this).val();
	        if(this.value) {
	        	$(".msg-insert").append("<br/>"+text);
	        	$(this).val('');
	        } else {
	        	alert('Masukan Kata!');
	        	$(this).val('');
	        }

	        
	    }
	});
	$('img#Expand-Arrow').on('click',function(e) {
	    $(".chat-body").hide();
	    $(this).hide();
	    $("img#Expand-Arrow-Up").show();
	});
	$('img#Expand-Arrow-Up').on('click',function(e) {
	    $(".chat-body").show();
	    $(this).hide();
	    $("img#Expand-Arrow").show();
	});

	var arr = [
			{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
			{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
			{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
			{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
			{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
			{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
			{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
			{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
			{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
			{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
			{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
  	];

  	var arr2 = {"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"}

  	var themes = JSON.stringify(arr);
  	var selectedTheme = JSON.stringify(arr2);

  	localStorage.themes = themes;
  	localStorage.selectedTheme = selectedTheme;
});

$(document).ready(function() {
			$('.my-select').select2({
				placeholder:"select",
				data:JSON.parse(localStorage.themes)
			});
});


$('.apply-button').on('click', function(){ // sesuaikan class button
	// [TODO] ambil Value dari elemen select .my-select
	var text = $(".my-select").val();	
	// [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
	list = JSON.parse(localStorage.themes);
	for (var i = 0; i < list.length ; i++) {
		if(list[i]['id'] == text) {
		// [TODO] ambil object theme yang dipilih
		theme = list[i];
		// [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
		$('body').css('background-color',theme['bcgColor']);
		$('.apply-button').css('color',theme['fontColor']);
		// [TODO] simpan object theme tadi ke local storage selectedTheme
		localStorage.selectedTheme = JSON.stringify(list[i]);
		}
	}

})
