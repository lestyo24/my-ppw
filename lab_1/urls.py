from django.urls import re_path
from .views import index,message_post,table

app_name = 'lab_1'

#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^table', table, name='table'),
    re_path(r'^message_post', message_post, name='message_post'),
]
