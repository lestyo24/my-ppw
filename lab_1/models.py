from django.db import models
from django.utils import timezone

class Message(models.Model):
	name = models.CharField(max_length=27)
	email = models.EmailField()
	message = models.TextField()
	created_date = models.DateTimeField(default=timezone.localtime())
	
	def str (self):
		return self.message