from django.shortcuts import render, redirect
from datetime import datetime, date
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
from django.contrib.auth import authenticate, login
# from whitenoise.django import DjangoWhiteNoise
from django.core.wsgi import get_wsgi_application

# application = get_wsgi_application()
# application = DjangoWhiteNoise(application)
# Enter your name here
mhs_name = 'Paulus Lestyo Adhiatma' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 3, 9) #TODO Implement this, format (Year, Month, Date)
npm = None # TODO Implement this
messages = Message.objects.all().values()



# Create your views here.

def index(request):
	response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
	response['message_form'] = Message_Form
	return render(request, 'index_lab1.html', response)

def table(request):
		response = {'name': 'Message'}
		response['message'] = messages
		if request.user.is_authenticated:
			return render(request, 'table.html', response)
		else:
			return redirect('login')

def calculate_age(birth_year):
	return curr_year - birth_year if birth_year <= curr_year else 0


def message_post(request):
	form = Message_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response = {}
		response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
		response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
		response['message'] = request.POST['message']
		message = Message(name=response['name'], email=response['email'],
		message=response['message'])
		message.save()
		html ='form_result.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/')