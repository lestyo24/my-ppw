"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab_1.views import index as index_lab1
from my_experience.views import index as index_my_experience
from my_skill.views import index as index_my_skill
from my_portfolio.views import index as index_my_portfolio
from lab_4.views import index as index_lab4


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-2/', include('lab_2.urls')),
    re_path(r'^$', index_lab1, name='index'),
    re_path(r'^my_experience', index_my_experience, name='my_experience'),
    re_path(r'^my_portfolio', index_my_portfolio, name='my_portfolio'),
    re_path(r'^my_skill', index_my_skill, name='my_skill'),
    re_path(r'^lab-4/', index_lab4, name='lab4'),
    re_path(r'^', include('django.contrib.auth.urls')),
    re_path(r'^', include('login.urls'))

]
