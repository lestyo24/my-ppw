from django.apps import AppConfig


class MySkillConfig(AppConfig):
    name = 'my_skill'
